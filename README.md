# vue-slim-scrolltop

A Simple ScrollTop Button Component for Vue.js, that scroll page to top when clicked.


## Features

- Make Floating ScrollTop Button Component on your project


## Installing

```bash
$ npm install vue-slim-scrolltop
```


## Usage

* Default Usage :
```js
<template>
  <div>
    <scroll-top />
  </div>
</template>

<script>
import ScrollTop from 'vue-slim-scrolltop';

export default {
  components: {
    ScrollTop,
  },
};
</script>

```

* If you wanna use custom settings, then : 
```js
<template>
  <div>
    <scroll-top
    :locationX="locationX"
    :locationY="locationY"
    :dataX="dataX"
    :dataY="dataY"
    :text="text" />
  </div>
</template>

<script>
import ScrollTop from 'vue-slim-scrolltop';

export default {
  components: {
    ScrollTop,
  },
  data() {
    return {
      locationX: 'left',
      locationY: 'bottom',
      dataX: 10,
      dataY: 10,
      text: 'Scroll to Top (Custom Text)',
    };
  },
};
</script>

```

## Props

| Name                | Type             | Default              | Description                                    |
|---------------------|------------------|----------------------|------------------------------------------------|
| locationX           | String           | 'right'              | X-Axis Type of position, like 'left', 'right'. |
| locationY           | String           | 'bottom'             | Y-Axis Type of position, like 'top', 'bottom'. |
| dataX               | String or Number | 80                   | X-Axis value of position                       |
| dataY               | String or Number | 80                   | Y-Axis value of position                       |
| text                | String           | 'Scroll Page to Top' | Text of button                                 |


## Author

**vue-slim-scrolltop** © [CHE5YA](https://github.com/che5ya), Released under the [MIT](./LICENSE) License.<br>

> GitHub [@che5ya](https://github.com/che5ya) · Facebook [@che5ya](https://www.facebook.com/che5ya) · Twitch [@chesya_](https://www.twitch.tv/chesya_)


## License

[MIT](LICENSE)
